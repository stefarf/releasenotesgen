package main

import (
	"time"

	"gitlab.com/stefarf/iferr"
)

var loc = mustLoadLocation("Asia/Jakarta")

func mustLoadLocation(location string) *time.Location {
	loc, err := time.LoadLocation(location)
	iferr.Panic(err)
	return loc
}

func convertCommitDate(date string) string {
	t, err := time.Parse("Mon Jan 2 15:04:05 2006 -0700", date)
	if err != nil {
		return "<unknown-date>"
	}
	return t.In(loc).Format("Mon Jan 2 2006")
}
