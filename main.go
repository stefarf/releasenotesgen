package main

import (
	"encoding/json"
	"errors"
	"flag"
	"fmt"
	"log"
	"os"
	"path"

	"gitlab.com/stefarf/iferr"
)

var (
	commitBranch   = mustGetEnv("CI_COMMIT_BRANCH")
	releaseVersion = mustGetEnv("VERSION")
	slackToken     = mustGetEnv("SLACK_TOKEN")
	slackChannel   = mustGetEnv("SLACK_CHANNEL")

	fAppId          = flag.String("appId", "", "short app id")
	fLastCommitFile = flag.String("lastCommitFile", "", "last commit json file")
)

func init() {
	flag.Parse()
	if *fAppId == "" {
		log.Fatal("requires: -appId")
	}
	if *fLastCommitFile == "" {
		log.Fatal("requires: -lastCommitFile")
	}
}

func main() {
	commits := gitLog(50)

	lastMap := map[string]bool{}
	if fileExist(*fLastCommitFile) {
		for _, commit := range readCommitsJsonFile(*fLastCommitFile) {
			lastMap[commit.Hash] = true
		}
	}

	var newReleaseCommits []Commit
	for _, commit := range commits {
		if !lastMap[commit.Hash] {
			newReleaseCommits = append(newReleaseCommits, commit)
		}
	}

	writeCommitsJsonFile(*fLastCommitFile, commits)

	text := ""
	if len(newReleaseCommits) != 0 {
		text = fmt.Sprintf("Release notes v%s:\n", releaseVersion)
		for _, c := range newReleaseCommits {
			text += fmt.Sprintf("%s, [%s] %s\n", c.Date, c.Author, c.Message)
		}
	} else {
		text = fmt.Sprintf("No release notes for v%s", releaseVersion)
	}
	slack(slackToken, slackChannel, commitBranch, *fAppId, text)
}

func fileExist(filename string) bool {
	info, err := os.Stat(filename)
	if err != nil {
		if errors.Is(err, os.ErrNotExist) {
			return false
		}
		panic(err)
	}
	if info.IsDir() {
		panic(fmt.Sprintf("%s is not a file", filename))
	}
	return true
}

func readCommitsJsonFile(filename string) []Commit {
	b, err := os.ReadFile(filename)
	iferr.Panic(err)
	var commits []Commit
	iferr.Panic(json.Unmarshal(b, &commits))
	return commits
}

func writeCommitsJsonFile(filename string, commits []Commit) {
	b, err := json.Marshal(commits)
	iferr.Panic(err)
	iferr.Panic(os.MkdirAll(path.Dir(filename), os.ModePerm))
	iferr.Panic(os.WriteFile(filename, b, os.ModePerm))
}
