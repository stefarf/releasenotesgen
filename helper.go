package main

import "os"

func mustGetEnv(key string) string {
	val := os.Getenv(key)
	if val == "" {
		panic("unknown environment variable: " + key)
	}
	return val
}
