package main

import (
	"strings"
)

type GitLogger struct {
	hash        string
	merge       bool
	author      string
	commitDate  string
	captureLine bool
	lines       []string

	Commits []Commit
}

func (gl *GitLogger) NewCommit(hash string) {
	gl.hash = hash
	gl.merge = false
	gl.author = "<no-author>"
	gl.commitDate = "<no-date>"
	gl.captureLine = false
	gl.lines = nil
}

func (gl *GitLogger) ItIsAMerge()                     { gl.merge = true }
func (gl *GitLogger) IsAMerge() bool                  { return gl.merge }
func (gl *GitLogger) SetAuthor(author string)         { gl.author = author }
func (gl *GitLogger) SetCommitDate(commitDate string) { gl.commitDate = commitDate }

func (gl *GitLogger) EmptyLine() {
	if !gl.captureLine {
		gl.captureLine = true
	} else {
		gl.Process()
	}
}

func (gl *GitLogger) IsCapture() bool  { return gl.captureLine }
func (gl *GitLogger) Line(line string) { gl.lines = append(gl.lines, line) }

func (gl *GitLogger) Process() {
	message := strings.Join(gl.lines, " ")
	if strings.HasPrefix(message, "Version: ") {
		return
	}
	gl.Commits = append(gl.Commits, Commit{
		Hash:    gl.hash,
		Author:  gl.author,
		Date:    gl.commitDate,
		Message: message,
	})
}
