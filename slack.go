package main

import (
	"fmt"

	"github.com/go-resty/resty/v2"
)

func slack(slackToken, slackChannel, commitBranch, name, text string) {
	type Body struct {
		Channel string `json:"channel"`
		Text    string `json:"text"`
	}
	client := resty.New()
	res, err := client.R().
		SetHeader("Authorization", "Bearer "+slackToken).
		SetBody(Body{
			Channel: slackChannel,
			Text:    fmt.Sprintf("[%s][%s] %s", name, commitBranch, text),
		}).
		Post("https://slack.com/api/chat.postMessage")
	fmt.Println(err)
	fmt.Println(res.StatusCode())
	fmt.Println(string(res.Body()))
}
