package main

import (
	"bytes"
	"fmt"
	"io"
	"os"
	"os/exec"
	"strings"

	"gitlab.com/stefarf/iferr"
)

type Commit struct {
	Hash    string
	Author  string
	Date    string
	Message string
}

func gitLog(n uint) []Commit {
	var buf bytes.Buffer
	c := exec.Command("git", "log", fmt.Sprintf("-%d", n), "--pretty=fuller")
	c.Stdout = &buf
	c.Stderr = os.Stderr
	iferr.Panic(c.Run())

	var gl GitLogger
	for {
		l, err := buf.ReadString('\n')
		switch err {
		case nil:
		case io.EOF:
			if !gl.IsAMerge() {
				gl.Process()
			}
			return gl.Commits
		default:
			panic(err)
		}

		if strings.HasPrefix(l, "commit ") {
			hash := strings.TrimSpace(strings.TrimPrefix(l, "commit "))
			gl.NewCommit(hash)
			continue
		}
		if strings.HasPrefix(l, "Merge: ") {
			gl.ItIsAMerge()
			continue
		}
		if gl.IsAMerge() {
			continue
		}

		if strings.HasPrefix(l, "Author: ") {
			author := strings.TrimPrefix(l, "Author: ")
			if i := strings.Index(author, "<"); i != -1 {
				author = strings.TrimSpace(author[:i])
			}
			gl.SetAuthor(author)
			continue
		}
		if strings.HasPrefix(l, "CommitDate: ") {
			commitDate := strings.TrimSpace(strings.TrimPrefix(l, "CommitDate: "))
			gl.SetCommitDate(convertCommitDate(commitDate))
			continue
		}

		if l == "\n" {
			gl.EmptyLine()
			continue
		}

		if gl.IsCapture() {
			gl.Line(strings.TrimSpace(l))
		}
	}
}
