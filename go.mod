module gitlab.com/stefarf/releasenotesgen

go 1.18

require (
	github.com/go-resty/resty/v2 v2.7.0
	gitlab.com/stefarf/iferr v0.1.1
)

require golang.org/x/net v0.0.0-20211030010937-7b24c0a3601d // indirect
